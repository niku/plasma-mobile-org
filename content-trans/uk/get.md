---
menu:
  main:
    name: Встановити
    weight: 4
sassFiles:
- scss/get.scss
title: Дистрибутиви, які пропонують Plasma Mobile
---
## Мобільні пристрої

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM є дистрибутивом Manjaro, але для пристроїв на основі ARM. Його засновано на Arch Linux ARM, поєднано із інструментами Manjaro, темами та інфраструктурою для встановлення образів для вашого пристрою ARM.

[Сайт](https://manjaro.org) [Форум](https://forum.manjaro.org/c/arm/)

##### Отримання

* [Найсвіжіша стабільна (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Збірки для розробників (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Встановлення

Щодо PinePhone, ви можете знайти загальні відомості у [вікі Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) — оптимізована для сенсорних пристроїв, попередньо налаштована версія Alpine Linux, яку можна встановити на смартфони та інші мобільні пристрої. Перегляньте [список пристроїв](https://wiki.postmarketos.org/wiki/Devices), щоб ознайомитися із поступом у підтримці вашого пристрою.

Для пристроїв, для яких немає попередньо зібраних образів, вам доведеться перешити систему пристрою за допомогою програми `pmbootstrap`. Виконайте настанови, які наведено [тут](https://wiki.postmarketos.org/wiki/Installation_guide). Не забудьте також ознайомитися зі сторінкою вікі пристрою, щоб дізнатися більше про те, що саме має працювати.

[Дізнатися більше](https://postmarketos.org)

##### Отримання

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Найсвіжіша (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Пристрої спільноти](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, раніше SUSE Linux і SuSE Linux Professional, — дистрибутив Linux, який спонсоровано SUSE Linux GmbH та іншими компаніями. У поточній версії openSUSE надає доступ до заснованих на Tumbleweed збірках Plasma Mobile.

##### Отримання

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Встановлення

Щодо PinePhone, ви можете знайти загальні відомості у [вікі Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Комп'ютерні пристрої

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Образ ISO для amd64 на основі Neon

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
