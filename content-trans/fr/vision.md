---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Vision
    parent: project
    weight: 1
title: Notre vision
---
Plasma Mobile a pour objectif de devenir un système de logiciels complets et libres pour les périphériques mobiles. Il est conçu pour redonner le contrôle sur leurs informations et communications aux utilisateurs soucieux de leurs vies privées.

Plasma Mobile adopte une approche pragmatique et est ouvert aux logiciels tiers, permettant aux utilisateurs de choisir quelles applications et quels services utiliser, tout en fournissant une expérience homogène sur plusieurs périphériques. <br/> Plasma Mobile implémente des standards ouverts et est développé selon un processus transparent, ouvert à toute personne souhaitant y participer.
