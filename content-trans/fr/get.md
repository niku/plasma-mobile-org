---
menu:
  main:
    name: Installer
    weight: 4
sassFiles:
- scss/get.scss
title: Distributions proposant Plasma Mobile
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM est une distribution Manjaro mais pour périphériques ARM. Elle repose sur Arch Linux ARM, associée à des outils, des thèmes et de l'infrastructure de Manjaro pour réaliser des installations pour votre périphérique ARM.

[Site Internet](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Télécharger

* [Dernière version stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Compilations pour développeurs (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installation

Pour le PinePhone, vous pouvez trouver des informations générales sur [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) est une distribution Linux Alpine, pré configurée et optimisée pour périphériques tactiles, pouvant être installée sur les téléphones portable et d'autres périphériques mobiles. Vous pouvez afficher une [liste de périphériques](https://wiki.postmarketos.org/wiki/Devices) pour voir l'avancement de la prise en charge de votre périphérique.

Pour les appareils n'ayant pas d'images pré construites, vous devrez le flasher manuellement en utilisant l'utilitaire « pmbootstrap ». Veuillez suivre les instructions [ici] (https://wiki.postmarketos.org/wiki/Installation_guide). Veuillez vérifiez également la page wiki de l'appareil pour plus d'informations sur ce qui fonctionne.

[En savoir plus](https://postmarketos.org)

##### Télécharger

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Version la plus récente (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Périphériques de la communauté](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### OpenSUSE

![](/img/openSUSE.svg)

OpenSUSE, anciennement SUSE Linux et SuSE Linux Professionnal, est un distribution Linux, développée par SUS Linux GmbH et d'autres entreprises. Actuellement, OpenSUSE fournit Tumbleweed, reposant sur les compilations de Plasma Mobile.

##### Télécharger

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Installation

Pour le PinePhone, vous pouvez trouver des informations générales sur [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Périphériques de bureau

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Image « ISO » reposant sur Neon en « amd 64 »

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
