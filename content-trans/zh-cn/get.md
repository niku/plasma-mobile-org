---
menu:
  main:
    name: 安装
    weight: 4
sassFiles:
- scss/get.scss
title: 提供 Plasma Mobile 的发行版
---
## 移动设备

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM 是支持 ARM 设备的 Manjaro 发行版。它的安装镜像基于 Arch Linux ARM 构建，配套有 Manjaro 的工具、主题和基础软件体系。

[网站](https://manjaro.org) | [论坛](https://forum.manjaro.org/c/arm/)

##### 下载：

* [最新稳定版 (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [开发测试版 (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### 安装

如果您使用的是 PinePhone，请在 [Pine64 百科网站](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions) 查找相关信息。

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) 是一款面向触摸优化、预先配置的 Alpine Linux 发行版，可以安装到安卓智能手机和其他移动设备。请[查看设备列表](https://wiki.postmarketos.org/wiki/Devices)确认您的设备的支持情况。

对于那些未能提供预构建镜像的设备，您必须使用 `pmbootstrap` 工具手动刷入该系统。请根据[安装指南](https://wiki.postmarketos.org/wiki/Installation_guide)进行操作，并记得检查设备对应的百科页面以了解设备的支持情况。

[了解详情](https://postmarketos.org)

##### 下载：

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [最新测试版 (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [社区维护版](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE，曾用名为 SUSE Linux 和 SuSE Linux 专业版，是一款由 SUSE Linux 公司和其他企业赞助开发的 Linux 发行版。目前 openSUSE 提供了基于 Tumbleweed 的 Plasma Mobile 系统镜像。

##### 下载：

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### 安装

如果您使用的是 PinePhone，请在 [Pine64 百科网站](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions) 查找相关信息。

## 桌面设备

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### KDE Neon amd64 ISO 系统镜像

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [KDE Neon amd64](https://files.kde.org/neon/images/mobile/)
