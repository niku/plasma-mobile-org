---
menu:
  main:
    name: Instalatu
    weight: 4
sassFiles:
- scss/get.scss
title: Plasma Mobile eskaintzen duten banaketak
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM, ARM gailuetarako Manjaro banaketa da. Arch Linux ARM-en oinarrituta dago, Manjaro tresnak, gaiak eta azpiegiturarekin konbinatuta, zure ARM gailuetarako irudiak instalatzeko.

[Webgunea](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Zama-jaitsi

* [Azken egonkorra (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Garatzaileentzako eraikinak] (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalazioa

PinePhone-rako, informazio generikoa aurki dezakezu [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)an.

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), telefono adimendunetan eta beste gailu mugikorretan instalatu daitekeen, ukimenerako optimizatutako, aurre-konfiguratutako Alpine Linux bat da. Begiratu [gailu-zerrenda](https://wiki.postmarketos.org/wiki/Devices) zure gailuak euskarria izateko aurrerapena ikusteko.

Aurre-eraikitako irudirik ez duten gailuetarako, «pmbootstrap» erabili beharko duzu irudia flash memorian eskuz grabatzeko. Jarraitu [honako](https://wiki.postmarketos.org/wiki/Installation_guide) jarraibideak. Egiaztatu gailuaren wiki orria funtzionatzen ari denari buruzko informazio gehiago lortzeko.

[Jakizu gehiago](https://postmarketos.org)

##### Zama-jaitsi

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [«Edge» berriena (PinePhone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Komunitateko gailuak](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, behinola SUSE Linux eta SuSE Linux Professional, SUSE Linux GmbH-ek eta beste konpainia batzuek babestutako Linux banaketa bat da. Gaur egun, openSUSE-k Tumbleweed oinarri duten Plasma Mobile eraikinak hornitzen ditu.

##### Zama-jaitsi

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instalazioa

PinePhone-rako, informazio generikoa aurki dezakezu [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)an.

## Mahaigaineko gailuak

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Neon oinarriko amd64 ISO irudia

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
