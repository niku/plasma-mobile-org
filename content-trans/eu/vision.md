---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Bisioa
    parent: project
    weight: 1
title: Gure Bisioa
---
Plasma Mobile-k, gailu mugikorretarako sistema oso eta software irekikoa izan nahi du.<br />Pribatutasunari erreparatzen dioten erabiltzaileei euren informazio eta komunikazioaren gaineko kontrola itzultzeko diseinatuta dago.

Plasma Mobile-k ikuspegi pragmatiko bat hartu, eta beste batzuen softwarea onartzen du, erabiltzaileari zer aplikazio eta zerbitzu erabili aukeratzeko parada emanez, aldi berean, gailu ezberdinen artean pitzadurarik gabeko esperientzia eskainiz.<br />Plasma Mobile-k estandar irekiak inplementatzen ditu eta prozesu garden batean garatzen da, parte hartzea guztientzako irekia delarik.
