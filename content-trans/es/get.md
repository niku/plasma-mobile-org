---
menu:
  main:
    name: Instalación
    weight: 4
sassFiles:
- scss/get.scss
title: Distribuciones que ofrecen Plasma Mobile
---
## Móvil

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM es la distribución Manjaro, pero para dispositivos ARM. Está basada en Arch Linux ARM, combinada con las herramientas de Manjaro, los temas y la infraestructura necesarios para crear imágenes que se puedan instalar en dispositivos ARM.

[Sitio web](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Descargar

* [Última estable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Compilaciones de desarrolladores (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalación

Puede encontrar información general sobre el PinePhone en [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), es un Alpine Linux preconfigurado y optimizado para pantallas táctiles que se puede instalar en *smartphones* y en otros dispositivos móviles. Consulte la [lista de dispositivos](https://wiki.postmarketos.org/wiki/Devices) para ver el estado de compatibilidad con su dispositivo.

Para los dispositivos que no disponen de imágenes precompiladas, tendrá que «flashearlas» de forma manual usando la utilidad `pmbootstrap`. Siga las instrucciones [aquí](https://wiki.postmarketos.org/wiki/Installation_guide). Asegúrese también de comprobar la página wiki del dispositivo para obtener más información sobre lo que funciona.

[Saber más](https://postmarketos.org)

##### Descargar

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Última versión «edge» (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Dispositivos de la comunidad](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, anteriormente SUSE Linux y SuSE Linux Professional, es una distribución Linux patrocinada por SUSE Linux GmbH y otras compañías. En la actualidad, openSUSE proporciona compilaciones de Plasma Mobile basadas en Tumbleweed.

##### Descargar

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instalación

Puede encontrar información general sobre el PinePhone en [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Dispositivos de escritorio

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Imagen ISO amd64 basada en Neon

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
