---
menu:
  main:
    name: Instal·leu-lo
    weight: 4
sassFiles:
- scss/get.scss
title: Distribucions que ofereixen el Plasma Mobile
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM és la distribució Manjaro, però per a dispositius ARM. Està basada en Arch Linux ARM, combinada amb les eines, temes i infraestructura de Manjaro per a fer la instal·lació d'imatges per als dispositius ARM.

[Lloc web](https://manjaro.org) [Fòrum](https://forum.manjaro.org/c/arm/)

##### Baixada

* [Darrera estable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Construccions de desenvolupador (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instal·lació

Per al PinePhone, es pot trobar informació genèrica al [wiki del Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), és un Alpine Linux tàctil optimitzat i preconfigurat que es pot instal·lar en telèfons intel·ligents i altres dispositius mòbils. Reviseu la [llista de dispositius](https://wiki.postmarketos.org/wiki/Devices) per a veure el progrés per acceptar el vostre dispositiu.

Per a dispositius que no tenen imatges preconstruïdes, cal gravar la seva memòria flaix interna manualment amb la utilitat `pmbootstrap`. Seguiu les instruccions [aquí](https://wiki.postmarketos.org/wiki/Installation_guide). Assegureu-vos també de verificar la pàgina wiki del dispositiu per a trobar més informació quant a què funciona.

[Apreneu-ne més](https://postmarketos.org)

##### Baixada

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Darrera «Edge» (PinePhone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Dispositius de la comunitat](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, antigament SUSE Linux i SuSE Linux Professional, és una distribució de Linux patrocinada per SUSE Linux GmbH i altres empreses. Actualment openSUSE proporciona construccions del Plasma Mobile basades en Tumbleweed.

##### Baixada

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instal·lació

Per al PinePhone, es pot trobar informació genèrica al [wiki del Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Dispositius d'escriptori

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

### Imatge ISO amd64 basada en Neon

![](/img/neon.svg)

WARNING: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
