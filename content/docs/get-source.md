---
title: Get the source code
weight: 2
---

## Frameworks

Plasma Mobile is built on top of [KDE Frameworks](https://projects.kde.org/projects/frameworks). You can find download links for the stable releases of these on [download.kde.org](http://download.kde.org/stable/frameworks/) and its [API documentation](http://api.kde.org/frameworks/) here.

## Plasma and Plasma Phone Component

Plasma provides components for multi-device workspaces. Plasma desktop
provides a stable, versatile and modular desktop.
Plasma Mobile shares many components and all of its underlying
architecture with Plasma desktop, but provides a UI and features
necessary and useful on mobile devices. The idea is to provide a base
platform, that can be augmented with device-specific modules. On a
laptop, one would typically install plasma-workspace and plasma-desktop,
while on a mobile device, one would install plasma-workspace and
plasma-mobile and friends. None of the repositories conflicts with
others, so it's possible to provide a system with UIs for multiple
formfactors, and decide at runtime, which UI to offer. You can find the
source code tarballs of the stable releases of Plasma on
[download.kde.org](http://download.kde.org/stable/plasma/) or check the git
repositories on [invent.kde.org](https://invent.kde.org/plasma).

## Applications

You can find the location of each code repository on the sidebar of
[apps.kde.org](https://apps.kde.org).

## Build the source code

You can find general information on how to build KDE software, on our
[Getting Started page](https://community.kde.org/Get_Involved/development).
We also have a section about [Plasma Mobile](https://community.kde.org/Get_Involved/development#Plasma_Mobile).

## Improving this documentation

Go to [invent.kde.org/websites/plasma-mobile-org](https://invent.kde.org/websites/plasma-mobile-org)
and help us making this documentation better.

